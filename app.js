// import Vue from 'vue'
// import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
	state: {
		todos: [{
				id: 1,
				task: 'Code',
				completed: true
			}, {
				id: 2,
				task: 'Sleep',
				completed: false
			}, {
				id: 3,
				task: 'Eat',
				completed: false
		}]
	},
	getters: {
		todos: state => state.todos
	},
	mutations: {
		// Add todo mutation
		addTodo: (state, payload) => {
			// Assemble data
			const task = {
				task: payload,
				completed: false
			}
			// Add to existing todos
			state.todos.unshift(task);
		},
		// Toggle Todo
		toggleTodo: (state, payload) => {
			console.log(payload);
			state.todos = state.todos.map(t => {
				if (t.id === payload) {
					// Update the todo
					// that matches the clicked item
					return {task: t.task, completed: !t.completed, id: t.id}
				}
				return t;
			})
		},
		deleteTodo: (state, payload) => {
			const index = state.todos.findIndex(t => t.id === payload);
			state.todos.splice(index, 1)
			console.log(index)
		}
	}
})

const todoList = {
	props: ['todos'],
	methods: {
		toggleTodo: function(id) {
			this.$store.commit('toggleTodo', id)
		},
		deleteTodo: function(id) {
			this.$store.commit('deleteTodo', id)
		}
	},
	template: `
    <div>
      <ul>
        <li v-for="t in todos" :class="{completed: t.completed}" @click="toggleTodo(t.id)" @dblclick="deleteTodo(t.id)">{{t.task}}</li>
      </ul>
    </div>
  `,
}

new Vue({
	el: '#app',
	data: function() {
		return {
			task: ''
		}
	},
	computed: {
		todos: function() {
			return this.$store.getters.todos
		}
	},
	methods: {
		addTodo: function() {
			// Commit to mutation
			this.$store.commit('addTodo', this.task)
			// Empty text input
			this.task = ''
		}
	},
	template: `
    <div>
      <form @submit.prevent="addTodo">
        <input type="text" v-model="task" />
      </form>
      <todo-list :todos="todos"></todo-list>
    </div>	
  `,
	store: store,
	components: {
		// Add child component to App
		'todo-list': todoList
	}
});

// Counter
// const store = new Vuex.Store({
// 	state: {
// 		count: 0
// 	},
// 	mutations: {
// 		increment: state => state.count++,
// 		decrement: state => state.count--
// 	}
// })
//
// new Vue({
// 	el: '#app',
// 	computed: {
// 		count () {
// 			return store.state.count
// 		}
// 	},
// 	methods: {
// 		increment () {
// 			store.commit('increment')
// 		},
// 		decrement () {
// 			store.commit('decrement')
// 		}
// 	}
// });
